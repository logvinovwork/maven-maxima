package life.maxima.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;


public class GCDTest {
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6})
    public void gcdLoopTest(int param){
        Assertions.assertEquals(param, GCD.gcdLoop(125,35));
    }

    @Test
    public void gcdRecursionTest(){
        Assertions.assertEquals(5, GCD.gcdRecursion(125, 35));
    }


}
